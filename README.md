# COMP90007 Workshop Slides

This repository contains the workshop slides of COMP90007 Internet Technologies offered at the University of Melbourne in Semester 1, 2019. 
The workshop slides are based on the official solutions released by the lecturer and the head tutor with minor modifications. 

The slide will be posted here at the end of each week. **Please attempt the questions first before looking at the solutions.**

**Please always refer to the official versions of the workshop solutions released on the LMS if you are looking for an 'authoritative' answer for any particular question**

## License

The slides are subject to copyright. For more information, please visit [the university copyright website](https://copyright.unimelb.edu.au/home#information).

## Good Luck With Your Exam! :smile: